package com.example.andreapowell.notebook;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NewNote extends AppCompatActivity {
    private DBAdapter record;
    private long ID;
    EditText title;
    EditText content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        record = new DBAdapter(this);

        title = findViewById(R.id.editTitle);
        content = findViewById(R.id.editContent);

        Bundle ext = getIntent().getExtras();
        if (ext != null) {
            String ti = ext.getString("Ti");
            String inf = ext.getString("Inf");

            title.setText(ti);
            content.setText(inf);
        }
    }

    public void save(View v) {
        Cursor chck = record.getData(title.toString());
        ID = chck.getLong(0);
        if (!chck.moveToFirst()) {
            record.insertData(title, content);
            Toast.makeText(getApplicationContext(), "File saved", Toast.LENGTH_SHORT).show();
        } else {
            record.updateData(ID, title, content);
            Toast.makeText(getApplicationContext(), "File updated",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /*public void delete(View v) {
    //TODO delete method

    }*/

}