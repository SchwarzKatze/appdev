package com.example.andreapowell.notebook;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.EditText;

import java.util.HashMap;

public class DBAdapter extends SQLiteOpenHelper {
    public static final String DBName = "DB_APP.db";
    public static final String DBTABLE_NAM = "DB_NOTE";
    public static final String KEY_ID = "_id";
    public static final String COLUMN_TITLE = "TITLE";
    public static final String COLUMN_INFO = "INFO";

    private static final String createSQL ="create table DB_NOTE " +
            "(_id integer primary key, TITLE text, INFO text)";

    public DBAdapter (Context context) {
        super(context, DBName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createSQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
        db.execSQL("drop table if exists DB_NOTE");
        onCreate(db);
    }

    public boolean insertData (EditText title, EditText info) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("TITLE", title.toString());
        content.put("INFO", info.toString());
        db.insert("DB_NOTE", null, content);
        return true;
    }

    public Cursor getData (String keyText) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.query(DBTABLE_NAM, new String[]{KEY_ID, COLUMN_TITLE, COLUMN_INFO},
                COLUMN_TITLE +"=?", new String[]{keyText},
                null,null,null);
        if (res != null) {
            res.moveToFirst();
        }
        return res;
    }

    public int updateData(long _id, EditText title, EditText info) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("TITLE", title.toString());
        content.put("INFO", info.toString());

        return db.update(DBName,content,"_id= " + _id,null);
    }

    public void deleteData(long _id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DBTABLE_NAM, "_id=" + _id, null);
    }

}
