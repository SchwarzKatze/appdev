package com.example.andreapowell.notebook;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    DBAdapter record;
    EditText search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        record = new DBAdapter(this);
        search = findViewById(R.id.searchText);
        search.setVisibility(View.GONE);

    }

    public void createFile(View v) {
        Intent page = new Intent(getApplicationContext(), NewNote.class);
        startActivity(page);
    }

    public void loadFile (View v) {
        search.setVisibility(View.VISIBLE);
        if (search.toString().trim().length() == 0) {
            Toast.makeText(getApplicationContext(), "Please enter s title",
                    Toast.LENGTH_SHORT).show();
        } else {
            String keyText = search.toString().trim();
            Cursor ans = record.getData(keyText);
            ans.moveToFirst();
            String ti = ans.getString(1);
            String inf = ans.getString(2);

            Intent page = new Intent(MainActivity.this, NewNote.class);
            page.putExtra("Ti",ti);
            page.putExtra("Inf",inf);
            startActivity(page);
        }

    }
}